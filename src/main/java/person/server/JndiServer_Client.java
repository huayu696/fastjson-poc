package person.server;

import java.rmi.Naming;
import java.rmi.Remote;

public class JndiServer_Client {
    /**
     * 客户端程序：通过 lookup()方法查找远程对象，进行远程方法调用
     * 查找远程对象并调用远程方法
     */
    public static void main(String[] argv) {
        try {
            Remote exploit = Naming.lookup("Exploit");
            System.out.println(exploit);

            //如果要从另一台启动了RMI注册服务的机器上查找hello实例
            //HelloInterface hello = (HelloInterface)Naming.lookup("//192.168.1.105:1099/Hello");

            //Remote lookup = Naming.lookup("127.0.0.1:1099/Exploit");
            //System.out.println(lookup);

            Remote lookup2 = Naming.lookup("rmi://127.0.0.1:1099/Exploit");
            System.out.println(lookup2);
        } catch (Exception e) {
            System.out.println("HelloClient exception: " + e);
        }
    }
}