package rmitest;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * 创建远程类：实现远程接口。
 * 扩展了UnicastRemoteObject类，并实现远程接口 HelloInterface , 非必须
 */
public class HelloInterfaceRemoteImpl extends UnicastRemoteObject implements HelloInterface {
    private String message;

    /**
     * 必须定义构造方法，即使是默认构造方法，也必须把它明确地写出来，因为它必须抛出出RemoteException异常
     */
    public HelloInterfaceRemoteImpl(String msg) throws RemoteException {
        System.out.println(" init construct method!");
        message = msg;
    }

    /**
     * 远程接口方法的实现
     */
    @Override
    public String say() throws RemoteException {
        System.out.println("Called by HelloClient");
        return "远程接口返回:" + message;
    }
}